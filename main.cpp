#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <queue>
#include <map>
#include <unordered_map>
#include <chrono>

template<typename T, typename Key = T>
class podatciClass {
public:
    void loadpodatci(const std::string &filename) {
        std::ifstream file(filename);
        if (!file.is_open()) {
            std::cerr << "Datoteka ne postoji: " << filename << std::endl;
            return;
        }

        std::string line;
        std::getline(file, line);
        int zapisi = 0;
        while (std::getline(file, line) && zapisi <= 500000) {

            auto row = splitLine(line);
            podatci.push_back(row);
            zapisi++;
            if (row.size() > 2) { //18
                unorderedMap[row[2]] = row;
                multimap.emplace(row[2], row);
            }

        }

        file.close();
    }
    //PRONALAZAK VRIJEDNOSTI
    std::vector<std::vector<T>> pronadiVrijednost(int Index, const T& vrijednost) const {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<std::vector<T>> nadeniRedci;

        for (const auto& row : podatci) {
            if (row.size() > Index && row[Index] == vrijednost) {
                nadeniRedci.push_back(row);
            }
        }

        for (const auto& row : nadeniRedci) {
            for (const auto& vrijed : row) {
                std::cout << vrijed << " ";
            }
            std::cout << std::endl;
        }

        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;

        return nadeniRedci;
    }


    std::vector<std::vector<T>> pronadiNVrijednosti(int Index, const T& vrijednost,int n) const {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<std::vector<T>> nadeniRedci;

        int brojac = 0;

        for (const auto& row : podatci) {
            if (row.size() > Index && row[Index] == vrijednost) {
                nadeniRedci.push_back(row);
                brojac++;
                if (brojac >= n) {
                    break;
                }
            }
        }

        for (const auto& row : nadeniRedci) {
            for (const auto& vrijed : row) {
                std::cout << vrijed << " ";
            }
            std::cout << std::endl;
            std::cout << std::endl;
        }

        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;

        return nadeniRedci;
    }
    //BRISANJE VRIJEDNOSTI
    void obrisiVrijednost(int Index, const T &vrijednost) {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<int> indexesToDelete;
        for (auto it = podatci.begin(); it != podatci.end();) {
            if (it->size() > Index && (*it)[Index] == vrijednost) {
                it = podatci.erase(it);
            } else {
                ++it;
            }
        }

        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;


    }

    void obrisiNVrijednosti(int Index, const T &vrijednost, unsigned int n) {
        auto start = std::chrono::high_resolution_clock::now();
        int brojac = 0;
        std::cout << "Vector size before loop: " << podatci.size() << std::endl; // Debugging output
        for (auto it = podatci.begin(); it != podatci.end() && brojac < n;) {
            if (it->size() > Index && (*it)[Index] == vrijednost) {
                it = podatci.erase(it);
                std::cout << "Obrisana je vrijednost: " << vrijednost << std::endl;
                ++brojac;
            } else {
                ++it;
            }
        }

        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
    }

    void obrisiKljuc(const Key& key) {
        auto start = std::chrono::high_resolution_clock::now();
        auto it = unorderedMap.find(key);
        if (it != unorderedMap.end()) {
            unorderedMap.erase(it);
            multimap.erase(key);
            std::cout << "Kljuc je " << key << " je obrisan" << std::endl;
        } else{
            std::cout << "Kljuc " << key << " Ne postoji" << std::endl;
        }
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
    }

    void obrisiNKljuceva(const Key &key, int n) {
        auto start = std::chrono::high_resolution_clock::now();
        auto it = unorderedMap.begin();
        while (it != unorderedMap.end() && n > 0) {
            if (it->first == key) {
                it = unorderedMap.erase(it);
                multimap.erase(key);
                std::cout << "Kljuc je " << key << " je obrisan" << std::endl;
                n--;
            } else {
                ++it;
            }

        }
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
    }

    // PRONALAZAK KLJUCEVA / N KLJUCEVA
    void pronadiKljuc(const T &key) const {
        auto start = std::chrono::high_resolution_clock::now();
        auto it = unorderedMap.find(key);
        if (it != unorderedMap.end()) {
            for (const auto &kljuc : it->second) {
                std::cout << kljuc << " ";
            }
        } else {
            std::cout << "Kljuc " << key << " Ne postoji" << std::endl;
        }
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
    }

    void pronadiNKljuceva(const Key &key, unsigned int n) const {
        auto start = std::chrono::high_resolution_clock::now();
        auto range = multimap.equal_range(key);
        int brojac = 0;
        std::cout << "Podatci vezani za kljuc: " << key << std::endl;
        for (auto it = range.first; it != range.second && brojac < n; it++, brojac++) {
            std::cout << std::endl;
            for (const auto& vrijednost : it->second) {
                for (const auto& value : vrijednost) {
                    std::cout << value << "";
                }
            }
            std::cout << std::endl;
        }
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
    }

    // DOHVACANJE / ISPISIVANJE VRIJEDNOSTI STUPACA
    std::vector<T> dohvatiVrijednostiStupca(int Index) const {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<T> VrijednostiStupca;


        for (const auto &row: podatci) {
            if (row.size() > Index) {
                VrijednostiStupca.push_back(row[Index]);
            }
        }
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
        return VrijednostiStupca;
    }

    void ispisiVrijednostiStupca(int Index) const {
        auto start = std::chrono::high_resolution_clock::now();

        std::vector<T> vrijednosttupca = dohvatiVrijednostiStupca(Index);

        std::cout << "Stupac: " << Index << ":";
        for (const auto &vrijednost : vrijednosttupca) {
            std::cout << "," << vrijednost;
        }
        std::cout << std::endl;
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
    }

    // Dohvaćanje MIN I MAX vrijednosti
    T dohvatiNajmanjuVrijednost(int Index) const {
        auto start = std::chrono::high_resolution_clock::now();


        std::priority_queue<T, std::vector<T>, std::greater<>> minHeap;
        for (const auto &row: podatci) {
            if (row.size() >= Index) {
                minHeap.push(row[Index]);
            }
        }
        std::cout << "Najmanja vrijednost u stupcu " << Index << " je: " << minHeap.top() << std::endl;
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
        return minHeap.top();
    }

    T dohvatiNajvecuVrijednost(int Index) const {
        auto start = std::chrono::high_resolution_clock::now();


        std::priority_queue<T> maxHeap;
        for (const auto &row: podatci) {
            if (row.size() >= Index) {
                maxHeap.push(row[Index]);
            }
        }
        std::cout << "Najveca vrijednost u stupcu " << Index << " je: " << maxHeap.top() << std::endl;
       
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
        return maxHeap.top();
    }

    std::vector<T> dohvatiNMinVrijednosti(int Index, unsigned int n) const {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<T> dohvacenaVrijednost;
        if (podatci.empty()) {
            return dohvacenaVrijednost;
        }

        std::priority_queue<T, std::vector<T>, std::greater<>> minHeap;
        for (const auto &row: podatci) {
            if (row.size() >= Index) {
                minHeap.push(row[Index]);
            }
        }

        while (!minHeap.empty() && dohvacenaVrijednost.size() < n) {
            dohvacenaVrijednost.push_back(minHeap.top());
            minHeap.pop();
        }
        std::cout << "Najmanja vrijednost u stupcu " << Index << " je: " << minHeap.top() << std::endl;
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
        return dohvacenaVrijednost;
    }

    std::vector<T> dohvatiNMaxVrijednosti(int Index, unsigned int n) const {
        auto start = std::chrono::high_resolution_clock::now();
        std::vector<T> dohvacenaVrijednost;
        if (podatci.empty()) {
            return dohvacenaVrijednost;
        }

        std::priority_queue<T> maxHeap;
        for (const auto &row: podatci) {
            if (row.size() > Index) {
                maxHeap.push(row[Index]);
            }
        }

        while (!maxHeap.empty() && dohvacenaVrijednost.size() < n) {
            dohvacenaVrijednost.push_back(maxHeap.top());
            maxHeap.pop();
        }
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;

        return dohvacenaVrijednost;
    }

    // OVDJE SU FUNKCIJE ZA PRINTANJE VRIJEDNOSTI!!
    void isprintajNMaxVrijednosti(int Index, unsigned int n) const {
        auto start = std::chrono::high_resolution_clock::now();
        std::priority_queue<T> maxHeap;
        for (const auto &row: podatci) {
            if (row.size() > Index) {
                maxHeap.push(row[Index]);
            }
        }

        std::cout << n << " Najvecih vrijednosti: ";
        while (!maxHeap.empty() && n > 0) {
            if (n > 1) {
                std::cout << maxHeap.top() << ",";
            } else {
                std::cout << maxHeap.top();
            }
            maxHeap.pop();
            n--;
        }
        std::cout << std::endl;
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
    }

    void isprintajNMinVrijednosti(int Index, unsigned int n) const {
        auto start = std::chrono::high_resolution_clock::now();
        std::priority_queue<T, std::vector<T>, std::greater<>> minHeap;
        for (const auto &row: podatci) {
            if (row.size() > Index) {
                minHeap.push(row[Index]);
            }
        }

        std::cout << n << " Najmanjih vrijednosti: ";
        while (!minHeap.empty() && n > 0) {
            if (n > 1) {
                std::cout << minHeap.top() << ",";
            } else {
                std::cout << minHeap.top();
            }
            minHeap.pop();
            n--;
        }
        std::cout << std::endl;
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
    }

    void dodajNoviRed(const std::vector<T>& noviRed) {
        auto start = std::chrono::high_resolution_clock::now();
        podatci.push_back(noviRed);

        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
    }



    std::unordered_map<Key, std::vector<T>> getunorderedmap() const {
        return unorderedMap;
    }

    void dodajZapisPoKljucu(const Key& key, const std::vector<T>& zapis) {
        auto start = std::chrono::high_resolution_clock::now();
        podatci.push_back(zapis);
        unorderedMap[key] = zapis;
        multimap.insert({key, zapis});
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
    }

    void dodajNZapisaPoKljucu(const Key& key, const std::vector<std::vector<T>>& zapisi) {
        auto start = std::chrono::high_resolution_clock::now();
        for (const auto& zapis : zapisi) {
            podatci.push_back(zapis);
            unorderedMap[key] = zapis;
            multimap.insert({key, zapis});
        }
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        std::cout << duration.count() << " ms" << std::endl;
    }


    void printKeys() const {
        std::cout << "Keys in unorderedMap:" << std::endl;
        for (const auto& pair : unorderedMap) {
            std::cout << pair.first << std::endl;
        }
        std::cout << "Keys in Multimap:" << std::endl;
        for (const auto& pair : multimap) {
            std::cout << pair.first << std::endl;
        }
    }





private:
    std::vector<std::vector<T>> podatci;

    std::unordered_map<Key, std::vector<T>> unorderedMap;
    std::unordered_multimap<Key, std::vector<T>> multimap;
    std::vector<T> splitLine(const std::string &line) const {
        std::vector<T> row;
        std::stringstream ss(line);
        T cell;

        while (std::getline(ss, cell, ',')) {
            row.emplace_back(cell);
        }

        return row;
    }
};

int main() {
    podatciClass<std::string> podatci;
    podatci.loadpodatci("fraudTest.csv");
    std::vector<std::string> zapis1 = {
            "555719", "trans_date_trans_time", "cc_num", "merchant", "category", "amt",
            "first", "last", "gender", "street", "city", "state", "zip", "lat",
            "long", "city_pop", "job", "dob", "unix_time", "merch_lat",
            "merch_long", "is_fraud"
    };

    std::vector<std::string> zapis2 = {
            "555720", "trans_date_trans_time", "cc_num", "merchant", "category", "amt",
            "first", "last", "gender", "street", "city", "state", "zip", "lat",
            "long", "city_pop", "job", "dob", "798db04aaceb4febd084f1a7c404da93", "unix_time", "merch_lat",
            "merch_long", "is_fraud"
    };

    //podatci.pronadiVrijednost(2,"30371006069917");
    //podatci.pronadiNVrijednosti(2,"30371006069917",5);
    //std::string maxVrijednost = podatci.dohvatiNajvecuVrijednost(21);

    //podatci.pronadiVrijednost(2,"30371006069917");
    //podatci.ispisiVrijednostiStupca(2);

    //std::vector<std::vector<std::string>> zapisi;
    //zapisi.push_back(zapis1);
    //zapisi.push_back(zapis2);
    //podatciClass<std::string> podaci;

    //podatci.printKeys();
   
    podatci.dodajZapisPoKljucu("798db04aaceb4febd084f1a7c404da93",zapis1);
    podatci.pronadiKljuc("798db04aaceb4febd084f1a7c404da93");



    //podatci.obrisiNKljuceva("3526826139003047",7);
    //podatci.obrisiKljuc("3521417320836166");
    //podatci.pronadiNKljuceva("3526826139003047", 5);


    /*DODAVANJE U NOVI RED
     *
    };

     podatci.dodajNoviRed(noviRed);

     DOHVACANJE I ISPISIVANJE POJEDINACNIH STUPACA
     std::vector<std::string> columnvrijednost = podatci.dohvatiVrijednostiStupca(1);
     podatci.ispisiVrijednostiStupca(5);


     TESTIRANJE BRISANJA VRIJEDNOSTI / N VRIJEDNOSTI
      podatci.obrisiVrijednost(2,"2da90c7d74bd46a0caf3777415b3ebd3");
      podatci.obrisiNVrijednosti(2,"30371006069917",10);


     TESTIRANJE PRONALASKA MAX VRIJEDNOSTI / ISPISIVANJA MAX VRIJEDNOSTI
     std::string maxVrijednost = podatci.dohvatiNajvecuVrijednost(21);
     std::string minVrijednost = podatci.dohvatiNajmanjuVrijednost(21);


     TESTIRANJE PRONALASKA N MAX VRIJEDNOSTI / ISPISIVANJA N MAX VRIJEDNOSTI
     std::vector<std::string> nMaxVrijednosti = podatci.dohvatiNMaxVrijednosti(4, 7);
     std::vector<std::string> nMinVrijednosti = podatci.dohvatiNMinVrijednosti(13, 7);

     podatci.isprintajNMaxVrijednosti(1, 7);
     podatci.isprintajNMinVrijednosti(1, 7);


     TESTIRANJE PRONALASKA / BRISANJA KLJUCA
     podatci.pronadiNKljuceva("3534330126107879", 5);
     podatci.pronadiKljuc("798db04aaceb4febd084f1a7c404da93");
     podatci.pronadiNKljuceva("798db04aaceb4febd084f1a7c404da93", 5);
     podatci.obrisiKljuc("798db04aaceb4febd084f1a7c404da93");
     podatci.pronadiKljuc("798db04aaceb4febd084f1a7c404da93");*/

    return 0;
}
